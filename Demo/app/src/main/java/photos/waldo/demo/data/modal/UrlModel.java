/*
 * Copyright (c) 2016 2Click Solutions. All rights reserved.
 * This file is a property of 2Click Solutions and its contents is confidential.
 * Redistribution or disclosure is strictly prohibited.
 *
 * Created by binhnguyen on 12/2/16.
 */
package photos.waldo.demo.data.modal;

public class UrlModel {
    public String url;
    public String size_code;
    public float quality;
    public int width;
    public int height;
}
