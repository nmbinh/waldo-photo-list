package photos.waldo.demo.presentation.view;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import photos.waldo.demo.presentation.presenter.AlbumPresenter;
import photos.waldo.demo.R;

public class AlbumActivity extends AppCompatActivity implements AlbumPresenter.AlbumViewHandler {
    RecyclerView recyclerView;
    TextView txtStatus;
    AlbumPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_album);
        recyclerView = (RecyclerView) findViewById(R.id.album_recyclerView);
        txtStatus = (TextView) findViewById(R.id.album_txtStatus);
        presenter = new AlbumPresenter(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        presenter.onStart();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.onDestroy();
    }

    @Override
    public Context getContext() {
        return getApplicationContext();
    }

    @Override
    public RecyclerView getRecyclerView() {
        return recyclerView;
    }

    @Override
    public void updateStatus(int dataSize) {
        if (dataSize > 0) {
            txtStatus.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);
        } else {
            txtStatus.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
            txtStatus.setText(R.string.text_nodata);
        }
    }

}
