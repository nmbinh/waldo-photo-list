/*
 * Copyright (c) 2016 2Click Solutions. All rights reserved.
 * This file is a property of 2Click Solutions and its contents is confidential.
 * Redistribution or disclosure is strictly prohibited.
 *
 * Created by binhnguyen on 12/2/16.
 */
package photos.waldo.demo.data.network;

import android.content.Context;
import android.support.annotation.IntRange;
import android.support.annotation.NonNull;
import android.support.annotation.Size;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import junit.framework.Assert;

import java.io.File;
import java.io.IOException;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import okhttp3.Cache;
import okhttp3.CacheControl;
import okhttp3.Call;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import photos.waldo.demo.data.modal.AlbumModel;
import rx.Observable;
import rx.subscriptions.Subscriptions;

public class DataProvider {
    /**
     * TODO: should move to gradle config when we development actual project.
     * I hardcode these value here because we focus on the photo list feature only.
     * In production code, we shouldn't hard-code in this way.
     * - The BASE_URL should be config via gradle build settings
     * - Cookie should be get and set automatically, when we may implement login feature
     */
    private static String BASE_URL = "https://core-graphql.staging.waldo.photos/gql";
    private static String cookie = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhY2NvdW50X2lkIjoiMmEyODQ4M2YtNWM2Yi00ZWU5LWE4YjUtYzFlMGU5NWUwYTY5Iiwicm9sZXMiOlsiYWRtaW5pc3RyYXRvciJdLCJpc3MiOiJ3YWxkbzpjb3JlIiwiZ3JhbnRzIjpbImFsYnVtczpkZWxldGU6KiIsImFsYnVtczpjcmVhdGU6KiIsImFsYnVtczplZGl0OioiLCJhbGJ1bXM6dmlldzoqIl0sImV4cCI6MTQ4MzIzNTU1MCwiaWF0IjoxNDgwNjQzNTUwfQ.eQhKoZRnJ05KSwie8W9A_nDDKs0CPfTsZBfSrGs9FFg";
    private static String queryForm = "{\n" +
            "  album(id: \"%s\") {\n" +
            "    id\n" +
            "    name\n" +
            "    photos(slice: {limit: %d, offset: %d}) {\n" +
            "    total" +
            "      records {\n" +
            "        id\n" +
            "        urls {\n" +
            "          size_code\n" +
            "          url\n" +
            "          width\n" +
            "          height\n" +
            "        }\n" +
            "      }\n" +
            "    }\n" +
            "  }\n" +
            "}";
    private OkHttpClient client = null;
    private Cache cache;
    private boolean useCache = false;

    private Call setupApiCaller(String query) {
        Assert.assertNotNull("You must call method openConnection first", client);

        HttpUrl url = HttpUrl.parse(BASE_URL)
                .newBuilder()
                .addQueryParameter("query", query)
                .build();
        //Prepare the api caller, within the cookie. The hard-code of cookie string will be
        //remove when we implement login feature.
        Call call = client.newCall(new Request.Builder()
                .cacheControl(new CacheControl.Builder().maxStale(useCache ? 24 : 0, TimeUnit.HOURS).build())
                .url(url)
                .addHeader("Cookie", "__staging.waldo.auth__=" + cookie)
                .build());
        return call;
    }

    /**
     * Call to close the opened network connection.
     * The only thing to do here is clear cache.
     */
    public void closeConnection() {
        try {
            if (cache != null)
                cache.delete();
            client = null;
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Open a network connection
     * The connection can support cache for server response.
     *
     * @param context  the application context
     * @param useCache true if want to use cache. The server response will be save in cache memory
     *                 for 24 hours by default. The cache also be clear if we close the connection.
     */
    public void openConnection(Context context, boolean useCache) {
        this.useCache = useCache;

        if (useCache) {
            int cacheSize = 10 * 1024 * 1024; // Hard code for 10MB cache. should be optimized on real project.
            File cacheDirectory = context.getCacheDir();
            cache = new Cache(cacheDirectory, cacheSize);
            client = new OkHttpClient.Builder().cache(cache).build();
        }
    }

    @NonNull
    public Observable<AlbumModel> loadAlbum(@NonNull String albumID,
                                            @Size(min = 10, max = 50) int pageSize,
                                            @IntRange(from = 0) int pageIndex) {
        final Call[] httpCaller = {null};
        return Observable.create(subscriber -> {

                    subscriber.add(Subscriptions.create(() -> {
                        /**
                         *This subscription will be notify if user unsubscribe the main subscription.
                         * That's the case we want to cancel api calling.
                         */
                        if (httpCaller[0] != null)
                            httpCaller[0].cancel();
                    }));

                    String query = String.format(Locale.getDefault(), queryForm, albumID, pageSize, pageIndex);
                    httpCaller[0] = setupApiCaller(query);

                    String jsonString;
                    try {
                        Response response = httpCaller[0].execute();
                        jsonString = response.body().string();
                        response.body().close();
                    } catch (Exception e) {
                        subscriber.onError(e);
                        return;
                    }
                    JsonObject jsonObject = new JsonParser().parse(jsonString).getAsJsonObject();
                    if (jsonObject.has("errors")) {
                        /**
                         * In case query not successful, we need to parse the error message manually
                         * then push to onError method.
                         */
                        String message = jsonObject.getAsJsonArray("errors")
                                .get(0)
                                .getAsJsonObject()
                                .get("message")
                                .getAsString();
                        subscriber.onError(new RuntimeException(message));
                        return;
                    }

                    JsonObject data = jsonObject.getAsJsonObject("data").getAsJsonObject("album");
                    AlbumModel albumModel = new Gson().fromJson(data, AlbumModel.class);
                    //Since we may run multiple server calling, we need to send back data segment
                    // offset, sothat subscriber knows where the data should be update to.
                    albumModel.photos.offset = pageIndex;
                    subscriber.onNext(albumModel);
                    subscriber.onCompleted();

                }
        );
    }
}
