/*
 * Copyright (c) 2016 2Click Solutions. All rights reserved.
 * This file is a property of 2Click Solutions and its contents is confidential.
 * Redistribution or disclosure is strictly prohibited.
 *
 * Created by binhnguyen on 12/3/16.
 */
package photos.waldo.demo.presentation.view;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.ImageView;

public class SquareImageView extends ImageView {
    public SquareImageView(Context context) {
        super(context);
    }

    public SquareImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public SquareImageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public SquareImageView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        //Always make the height dimension equals the width dimension. And, width dimension is divided
        //by grid layout manager. This way, we can display gird of equal square image items.
        super.onMeasure(widthMeasureSpec, widthMeasureSpec);
    }
}
