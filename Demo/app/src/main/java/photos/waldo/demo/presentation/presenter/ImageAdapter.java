/*
 * Copyright (c) 2016 2Click Solutions. All rights reserved.
 * This file is a property of 2Click Solutions and its contents is confidential.
 * Redistribution or disclosure is strictly prohibited.
 *
 * Created by binhnguyen on 12/1/16.
 */
package photos.waldo.demo.presentation.presenter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import photos.waldo.demo.R;
import photos.waldo.demo.presentation.view.ImageItemViewHolder;

class ImageAdapter extends RecyclerView.Adapter<ImageItemViewHolder> {
    /**
     * The list of image urls.
     */
    List<String> dataList = new ArrayList<>();

    @Override
    public ImageItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_image_item, parent, false);
        return new ImageItemViewHolder(itemView);
    }

    @Override
    public int getItemViewType(int position) {
        return 0;
    }

    @Override
    public void onBindViewHolder(ImageItemViewHolder holder, int position) {
        holder.loadImage(dataList.get(position));
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }
}
