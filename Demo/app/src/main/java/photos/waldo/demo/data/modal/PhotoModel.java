/*
 * Copyright (c) 2016 2Click Solutions. All rights reserved.
 * This file is a property of 2Click Solutions and its contents is confidential.
 * Redistribution or disclosure is strictly prohibited.
 *
 * Created by binhnguyen on 12/2/16.
 */
package photos.waldo.demo.data.modal;

public class PhotoModel {
    public int total;
    public int offset;
    public ImageModel[] records;
}