/*
 * Copyright (c) 2016 2Click Solutions. All rights reserved.
 * This file is a property of 2Click Solutions and its contents is confidential.
 * Redistribution or disclosure is strictly prohibited.
 *
 * Created by binhnguyen on 12/5/16.
 */
package photos.waldo.demo.presentation.view;

import android.content.Context;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;

/**
 * This custom layout manager wraps the built-in layout manager to skip crash when we update the
 * recyclerview concurrently in many threads.
 */
public class ThreadSafeGridLayoutManager extends GridLayoutManager {
    public ThreadSafeGridLayoutManager(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    public ThreadSafeGridLayoutManager(Context context, int spanCount) {
        super(context, spanCount);
    }

    public ThreadSafeGridLayoutManager(Context context, int spanCount, int orientation, boolean reverseLayout) {
        super(context, spanCount, orientation, reverseLayout);
    }

    @Override
    public void onLayoutChildren(RecyclerView.Recycler recycler, RecyclerView.State state) {
        try {
            super.onLayoutChildren(recycler, state);
        } catch (IndexOutOfBoundsException e) {
            e.printStackTrace();
        }
    }
}
