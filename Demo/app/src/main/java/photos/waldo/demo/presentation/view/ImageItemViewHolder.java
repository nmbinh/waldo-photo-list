/*
 * Copyright (c) 2016 2Click Solutions. All rights reserved.
 * This file is a property of 2Click Solutions and its contents is confidential.
 * Redistribution or disclosure is strictly prohibited.
 *
 * Created by binhnguyen on 12/1/16.
 */
package photos.waldo.demo.presentation.view;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import photos.waldo.demo.R;

public class ImageItemViewHolder extends RecyclerView.ViewHolder {
    private ImageView imageView;

    public ImageItemViewHolder(View itemView) {
        super(itemView);
        imageView = (ImageView) itemView.findViewById(R.id.imageItem);
    }

    public void loadImage(String imageUrl) {
        /**
         * If the image url is loaded yet, show grey background only.
         */
        if (imageUrl == null)
            imageView.setImageDrawable(null);
        else
            Picasso.with(imageView.getContext())
                    .load(imageUrl)
                    .fit()
                    .centerCrop()
                    .into(imageView);
    }
}
