/*
 * Copyright (c) 2016 2Click Solutions. All rights reserved.
 * This file is a property of 2Click Solutions and its contents is confidential.
 * Redistribution or disclosure is strictly prohibited.
 *
 * Created by binhnguyen on 12/1/16.
 */
package photos.waldo.demo.presentation.presenter;

import android.content.Context;
import android.graphics.Point;
import android.os.Handler;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SimpleItemAnimator;
import android.util.Log;
import android.view.Display;
import android.view.WindowManager;

import photos.waldo.demo.R;
import photos.waldo.demo.data.modal.PhotoModel;
import photos.waldo.demo.data.modal.UrlModel;
import photos.waldo.demo.data.network.DataProvider;
import photos.waldo.demo.presentation.view.ThreadSafeGridLayoutManager;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.Subscriptions;

public class AlbumPresenter extends RecyclerView.OnScrollListener {
    private static final String ALBUM_ID = "YWxidW06YTQwYzc5ODEtMzE1Zi00MWIyLTk5NjktMTI5NjIyZDAzNjA5";
    private static final int PAGE_SIZE = 50;
    private static final int SCROLL_THRESHOLD = 10;
    private static final int NUM_OF_COLUMN = 3;
    private static final int VISIBLE_THRESHOLD = 30;
    private static final int DATA_THRESHOLD = 2;

    private ImageAdapter adapter;
    private ThreadSafeGridLayoutManager layoutManager;
    private int totalDataItem = 0;

    private Subscription nextLoadingSubscriptions = Subscriptions.empty();
    private Subscription prevLoadingSubscriptions = Subscriptions.empty();
    private int nextLoadingOffset = -1;
    private int prevLoadingOffset = -1;

    private DataProvider dataProvider;

    private int imageViewSize = 0;

    public interface AlbumViewHandler {
        Context getContext();

        RecyclerView getRecyclerView();

        void updateStatus(int dataSize);
    }

    private AlbumViewHandler viewHandler;

    public AlbumPresenter(AlbumViewHandler viewHandler) {
        this.viewHandler = viewHandler;
        adapter = new ImageAdapter();
        viewHandler.getRecyclerView().setAdapter(adapter);
        viewHandler.getRecyclerView().addOnScrollListener(this);

        //Config to display 3 columns of images.
        layoutManager = new ThreadSafeGridLayoutManager(viewHandler.getContext(), NUM_OF_COLUMN);
        viewHandler.getRecyclerView().setLayoutManager(layoutManager);
        int spacingInPixels = viewHandler.getContext().getResources().getDimensionPixelSize(R.dimen.spacing);
        viewHandler.getRecyclerView().addItemDecoration(new SpacesItemDecoration(spacingInPixels));
        //fix blinking issue when calling notifyDataSetChange()
        ((SimpleItemAnimator) viewHandler.getRecyclerView().getItemAnimator()).setSupportsChangeAnimations(false);
        viewHandler.getRecyclerView().getRecycledViewPool().setMaxRecycledViews(0, 0);

        //load data for first segment
        calculateImageViewSize();
        dataProvider = new DataProvider();
        dataProvider.openConnection(viewHandler.getContext(), true);
        loadData(1);
    }

    public void onStart() {

    }

    public void onDestroy() {
        dataProvider.closeConnection();
        nextLoadingSubscriptions.unsubscribe();
        prevLoadingSubscriptions.unsubscribe();
    }

    private void calculateImageViewSize() {
        WindowManager wm = (WindowManager) viewHandler.getContext().getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;
        int spacingInPixels = viewHandler.getContext().getResources().getDimensionPixelSize(R.dimen.spacing);
        imageViewSize = (width - spacingInPixels * 4) / 3;
    }

    private Subscription loadData(int segmentOffset) {
        Log.d("TEST", "load data for segment " + segmentOffset);
        return dataProvider.loadAlbum(ALBUM_ID, PAGE_SIZE, segmentOffset)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .map(albumModel -> albumModel.photos)
                .subscribe(new Subscriber<PhotoModel>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                    }

                    @Override
                    public void onNext(PhotoModel photoSet) {
                        int segment = photoSet.offset;
                        setTotalDataItem(photoSet.total);
                        int startPos = (segment - 1) * PAGE_SIZE;
                        for (int i = 0; i < photoSet.records.length; i++) {
                            int position = startPos + i;
                            adapter.dataList.set(position, getFitImage(photoSet.records[i].urls));
                        }

                        //Notify adapter to refresh the view if it is visible to user.
                        new Handler().post(() -> adapter.notifyItemRangeChanged(
                                layoutManager.findFirstVisibleItemPosition() - NUM_OF_COLUMN,
                                layoutManager.findLastVisibleItemPosition() + NUM_OF_COLUMN));
                    }
                });
    }

    private void setTotalDataItem(int total) {
        if (totalDataItem == 0) {
            totalDataItem = total;
            addDataPlaceHolder();
            viewHandler.updateStatus(total);
        }
    }

    private String getFitImage(UrlModel[] urlModels) {
        for (UrlModel url : urlModels) {
            if (url.width >= imageViewSize) {
                return url.url;
            }
        }
        //if not found appropriate size, get the largest
        return urlModels[urlModels.length - 1].url;
    }

    @Override
    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
        super.onScrolled(recyclerView, dx, dy);
        //reduce the checking rate.
        if (Math.abs(dy) < SCROLL_THRESHOLD) return;

        int firstVisiblePos = layoutManager.findFirstVisibleItemPosition();
        int lastVisiblePos = layoutManager.findLastVisibleItemPosition();

        //when user scrolls over the adapter's size, add more place holder for smooth scrolling.
        if ((lastVisiblePos + VISIBLE_THRESHOLD) >= layoutManager.getItemCount()
                && adapter.dataList.size() < totalDataItem) {
            addDataPlaceHolder();
        }

        /**
         * If next segment items are still empty (we add place holders before), and it's data are not
         * in downloading then call server to full fill data items.
         *
         * In case user scroll very fast, we may call multiple downloading tasks concurrently.
         * So, we should cancel previous downloading tasks if they don't completed yet. By this way
         * we can save data-bandwidth. Only download data for segments that need to be display
         */
        int nextOffset = (lastVisiblePos + VISIBLE_THRESHOLD) / PAGE_SIZE + 1;
        if (lastVisiblePos + VISIBLE_THRESHOLD <= totalDataItem) {
            if (adapter.dataList.get(lastVisiblePos + VISIBLE_THRESHOLD) == null
                    && nextOffset != nextLoadingOffset) {
                nextLoadingOffset = nextOffset;
                nextLoadingSubscriptions.unsubscribe();
                nextLoadingSubscriptions = loadData(nextOffset);
            }
        }

        /**
         * This block handle for previous segment, with same logic as above.
         *
         */
        int prevOffset = nextOffset - 1;
        if (firstVisiblePos >= VISIBLE_THRESHOLD && prevOffset > 0) {
            if (adapter.dataList.get(firstVisiblePos - VISIBLE_THRESHOLD) == null
                    && prevOffset != prevLoadingOffset) {
                prevLoadingOffset = prevOffset;
                prevLoadingSubscriptions.unsubscribe();
                prevLoadingSubscriptions = loadData(prevOffset);
            }
        }


        //clear data for too old segments
        clearDataForSegment(prevOffset - DATA_THRESHOLD);
        clearDataForSegment(nextOffset + DATA_THRESHOLD);
    }

    /**
     * We will use this method to add data place holder for the recycler-view before it is actually loaded.
     * Whenever user scroll to bottom, we will add placeholders at the same time we call API to get
     * next data segment. These placeholders will be replace by actual data object when they returned
     * from server.
     */

    private void addDataPlaceHolder() {
        int currentSize = adapter.dataList.size();
        for (int i = 0; i < PAGE_SIZE && adapter.dataList.size() < totalDataItem; i++) {
            adapter.dataList.add(null);
        }
        int addCounter = adapter.dataList.size() - currentSize;

        //To fix the issue which can't notify data change when scrolling.
        new Handler().post(() -> adapter.notifyItemRangeInserted(currentSize, addCounter));
    }

    /**
     * Clear data for segments that too far from current visible segment. If not, the app may run
     * into Out-Of-Memmory issue.
     * <p>
     * Whenever user scroll back to removed segments, they will be loaded from cache memory.
     *
     * @param segment the index of old segment that need to be remove
     */
    private void clearDataForSegment(int segment) {
        int pos = segment * PAGE_SIZE;
        if (pos >= 0 && pos + PAGE_SIZE < adapter.dataList.size())
            for (int i = 0; i < PAGE_SIZE; i++) {
                adapter.dataList.set(pos + i, null);
            }
    }
}
