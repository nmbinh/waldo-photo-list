package photos.waldo.demo.data.network;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Matchers;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import okhttp3.Call;
import okhttp3.OkHttpClient;

@RunWith(PowerMockRunner.class)
@PrepareForTest({DataProvider.class, OkHttpClient.class, Call.class})
public class DataProviderTest {
    OkHttpClient mockClient;
    Call mockWebCaller;

    @Before
    public void setup() {
        try {
            mockClient = PowerMockito.mock(OkHttpClient.class);
            mockWebCaller = PowerMockito.mock(Call.class);
            PowerMockito.whenNew(OkHttpClient.class).withNoArguments().thenReturn(mockClient);
            PowerMockito.when(mockClient.newCall(Matchers.any())).thenReturn(mockWebCaller);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Check if network error was handled
     *
     * @throws Exception
     */
    @Test
    public void loadAlbum() throws Exception {
//        Context context = PowerMockito.mock(Context.class);
//        PowerMockito.when(context.getCacheDir()).thenReturn(new File("/"));
//        PowerMockito.when(mockWebCaller.execute()).thenThrow(new IOException("Mock Error"));
//        DataProvider dataProvider = new DataProvider();
//        dataProvider.openConnection(context);
//        Observable<AlbumModel> observable = DataProvider.loadAlbum("any id", 10, 1);
//        TestSubscriber<AlbumModel> subscriber = new TestSubscriber<>();
//        observable.subscribe(subscriber);
//
//        subscriber.assertError(IOException.class);
//        subscriber.assertTerminalEvent();
    }

}