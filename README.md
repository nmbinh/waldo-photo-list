# README #

This project is to implement a small task of Waldo Photos

Task description:
`https://gist.github.com/alwaysunday/9ace4a2938a7dbfa29465a257fd4b416`

### Source code structure ###
  ..**/presentation/view**: This package contains 1 activity and 3 custom views.

  - AlbumActivity: the unique activity of this project. It contains a recyclerview control, which
  displays a grid of image view-items.

  - ImageItemViewHolder: Presents an view-item of the main recycler-view. For demo purpose, it's
    contains a unique view inside (SquareImageView) without any action.

  - SquareImageView: A tiny custom image view which always draw itself as square. It's actual size
  will be divided based on screen width automatically.

  - ThreadSafeGridLayoutManager: This class extends the built-in GridLayoutManager, and catchs the
  exception when we update recycler-view data from many different threads.

    >_If we change the recycler-view's data from multiple threads at a time, the Layout Manager may
throws a `java.lang.IndexOutOfBoundsException` exceptions. There is no way to fix this bug at this time.
Create this class to cache the exception is just a workaround._


..**/presentation/presenter** : Here are classes implement ui-logic of application

- AlbumPresenter: The main job of this class is handle user scrolling behavior and call DataProvider
to load appropriate data-segments
- ImageAdapter: The boilerplate class that provides data to recycler view.
- SpacesItemDecoration: The decorator of recycler-view. It maintains space between view items in recycler view

..**/data/network**

 - DataProvider: This class provide methods to request data from server.

..**/data/model** : Data models are here.

### The main ideas ###
1. **Infinite scrolling:**

    Imagine that all images of the album are divided into multiple data-segments. We won't download all
  segments at beginning. When user scrolls down to view more images, we will send a request to server
  to get needed segments.

  To make the view scrolls smoothly, we will add place-holders for new segment before we actually
  get data from server. This means that users will see empty view-items before data are loaded.
  Right after we got data from server, they will be updated into appropriate position and the views
  will be refreshed to show images.

  Of course, if the total size of all segments is equals to total images of the album, we won't add
  place holders any more.


2. **Handle crazy scrolling**

    If users scroll the image list to view slowly (this is normal behavior), the application will
    download data segments one by one, until it reaches the end of the album. If user scroll very
    quickly, it won't.

    The idea is: The application shouldn't download segments that have rarely chance to display. Thus,
    It maintains only 2 downloading threads at a time.

    >2 threads: 1 for visible segment and 1 for next segment. Because the server take 2-3 seconds to returns
    data, we need to prepare a buffer. The number of concurrent downloading threads may be specified
    in production project, based on actual situations. the number-2 isn't the best.

    For example, the app starts with **segment 1**, and users scroll down quickly again and again to view
    **segment 10**. During scrolling, it may start many downloading threads concurrently
    (for segment 3, 4, 5, ... 9, 10, 11). But, it only maintains 2 latest threads. All previous ones
     will be canceled before they finish. Of course, if user scroll up to view previous segments,
     they will be download at that time.

3. **Caching and resolve Out of memory issue.**

    With thousands of image (even more), if the app keeps all data segments in memory, it may run into
     out of memory issue. Thus, it have to clear out data segments that were scrolled far away visible
     position. ( we set null to the data-list, and then Garbage Collector will do remaining job)

    When user scrolls back to cleared segments, It won't call server to re-download data. These data
    segments should be loaded from cache memory.

    The image items are loaded by 3rd library, call `Picasso`. This library already handle a cache memory
    for images, so we don't worry about it. The thing we should handle is caching API responses.

    One more important note about API caching: It should valid in current activity context only. Means,
     if we close the activity, we should clear it. So, the later time we open the activity, we'll call
     server to get latest data again.


